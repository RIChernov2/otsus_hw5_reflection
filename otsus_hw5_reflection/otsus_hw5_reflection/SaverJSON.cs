﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otsus_hw5_reflection
{
    internal class SaverJSON<T>
    {
        private static readonly string _fileName = "info.json";
        private readonly WorkerJSON<T> _workerJSON;
        public SaverJSON ()
        {
            _workerJSON = new WorkerJSON<T>();
        }
        public void Serialise(T objectT)
        {
            string stringJSON = _workerJSON.Serialise(objectT);
            File.WriteAllText(_fileName, stringJSON);
        }
        public T Deserialise()
        {
            if ( !File.Exists(_fileName) )
            {
                File.WriteAllText(_fileName, "");
            }

            var text = File.ReadAllText(_fileName);
            return _workerJSON.Deserialise(text);
        }
    }
}
