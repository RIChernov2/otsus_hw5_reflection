﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otsus_hw5_reflection
{
    internal class F
    {
        [JsonProperty("Int")]
        public int Int { get; set; }

        [JsonProperty("String")]
        public string String { get; set; }

        [JsonProperty("Bool")]
        public bool Bool { get; set; }

        [JsonProperty("Decimal")]
        public decimal Decimal;

        [JsonProperty("Float")]
        public float Float;

        [JsonProperty("Long")]
        public long Long { get; set; }

        [JsonProperty("Double")]
        public double Double { get; set; }

        public F()
        {
            Int = default;
            String = default;
            Bool = default;
            Decimal = default;
            Float = default;
            Long = default;
            Double = default;
        }

        public override string ToString()
        {
            return $"Int: {Int}; String: \"{String}\"; Bool: {Bool}; Decimal: {Decimal}; Float: {Float}; Long: {Long}; Double: {Double}";
        }

        public override bool Equals(object obj)
        {
            return obj is F f &&
                   Int == f.Int &&
                   String == f.String &&
                   Bool == f.Bool &&
                   Decimal == f.Decimal &&
                   Float == f.Float &&
                   Long == f.Long &&
                   Double == f.Double;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Int, String, Bool, Decimal, Float, Long, Double);
        }
    }
}
