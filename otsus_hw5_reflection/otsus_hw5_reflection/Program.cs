﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace otsus_hw5_reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            
            F f1 = new F()
            {
                Int = 12,
                String = "oops",
                Bool = true,
                Decimal = 500_100.001m,
                Float = 123.4f,
                Long = 33,
                Double = 12.3
            };

            F f2 = new F()
            {
                Int = 21,
                String = "soup",
                Bool = false,
                Decimal = 100_500.001m,
                Float = 321.4f,
                Long = 33,
                Double = 32.1
            };
            List<F> objectsF = new List<F>() { f1, f2 };

            F startf1 = f1;
            F startf2 = f2;

            int iterationCount1 = 1_000_000; // для циклов без сохранения
            int iterationCount2 = 10_000;// для циклов с  сохранением
            Stopwatch watch = new Stopwatch();
            TimeSpan time;

            Console.WriteLine($"Операции без сохранения в файл выполняются циклом {iterationCount1} раз:");
            // ************************ МОЙ CSV преобразование объект-строка *****************************
            watch.Start();
            WorkerCSV<F> worker = new WorkerCSV<F>("$");
            string formatcsv;
            for ( int i = 0 ; i < iterationCount1 ; i++ )
            {
                formatcsv = worker.Serialise(f1);
                f1 = worker.Deserialise(formatcsv);
            }
            watch.Stop();
            time = watch.Elapsed;
            Console.WriteLine($"Время сохранения \"Объект - строка CSV - Объект\" => {time.TotalSeconds} секунд(ы)");
            


            // ************************ JSON преобразование объект-строка *****************************
            watch.Reset();
            watch.Start();
            WorkerJSON<F> workerJSON = new WorkerJSON<F>();
            for ( int i = 0 ; i < iterationCount1 ; i++ )
            {
                formatcsv = workerJSON.Serialise(f1);
                f1 = workerJSON.Deserialise(formatcsv);
            }
            watch.Stop();
            time = watch.Elapsed;
            Console.WriteLine($"Время сохранения \"Объект - строка JSON - Объект\" => {time.TotalSeconds} секунд(ы)");


            // ************************ Замер выводла на консоль
            string serialisedCSV = formatcsv = worker.Serialise(f1);

            watch.Reset();
            watch.Start();
            Console.WriteLine($"\nСериализованная CSV строка {serialisedCSV}");
            watch.Stop();
            time = watch.Elapsed;
            Console.WriteLine($"Время вывода строки на консоль:\t {time.TotalSeconds} секунд(ы)\n");


            Console.WriteLine($"\nОперации с сохранением в файл выполняются циклом {iterationCount2} раз:");
            // ************************ МОЙ CSV с сохранением в файл *****************************
            watch.Reset();
            watch.Start();
            ListWorkerCSV<F> LWorkerCSV = new ListWorkerCSV<F>("$");
            for ( int i = 0 ; i < iterationCount2 ; i++ )
            {
                LWorkerCSV.Serialise(objectsF);
                objectsF = LWorkerCSV.Deserialise();
            }
            watch.Stop();
            time = watch.Elapsed;
            Console.WriteLine($"Время сохранения \"Лист объектов -  файл CSV - Лист объектов\" => {time.TotalSeconds} секунд(ы)");

            // ************************ JSON с сохранением в файл *****************************

            watch.Reset();
            watch.Start();
            SaverJSON<List<F>> saverJSON = new SaverJSON<List<F>>();
            for ( int i = 0 ; i < iterationCount2 ; i++ )
            {
                saverJSON.Serialise(objectsF);
                objectsF = saverJSON.Deserialise();
            }
            watch.Stop();
            time = watch.Elapsed;
            Console.WriteLine($"Время сохранения \"Лист объектов - файл JSON - Лист объектов\" => {time.TotalSeconds} секунд(ы)");

            // ************************ Сравнение объектов до и после манипуляций *****************************
            Console.WriteLine($"\nСтартовый объект равен объекту после манипуляций => {startf1.Equals(f1)}");
            Console.ReadLine();
        }       
    }
}
