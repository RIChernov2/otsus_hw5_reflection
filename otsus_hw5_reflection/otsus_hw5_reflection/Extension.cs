﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace otsus_hw5_reflection
{
    internal static class Extension
    {
        internal static object ParseValue(this string stringValue, Type typeWeNeed)
        {
            return Convert.ChangeType(stringValue, typeWeNeed);
        }

        public static void SetParseValue(this PropertyInfo propertyInfo, object? obj, string stringValue)
        {
            propertyInfo.SetValue(obj, stringValue.ParseValue(propertyInfo.PropertyType));
        }

        public static void SetParseValue(this FieldInfo fieldInfo, object? obj, string stringValue)
        {
            fieldInfo.SetValue(obj, stringValue.ParseValue(fieldInfo.FieldType));
        }
    }
}
