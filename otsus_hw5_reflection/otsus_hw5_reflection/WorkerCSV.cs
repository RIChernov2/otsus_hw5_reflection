﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace otsus_hw5_reflection
{
    internal class WorkerCSV <T>
        where T: class
    {
        private readonly string separator;

        public WorkerCSV(string separator = ",")
        {
            this.separator = separator;
        }

        public T Deserialise(string line)
        {
            string[] values = line
                                .Replace(separator, "\t")
                                .Replace(".", ",")
                                .Split("\t");

            Type objectF = typeof(T);
            int countOFFieldsAndPros = objectF.GetProperties().Length + objectF.GetFields().Length;
            if ( values.Count() != countOFFieldsAndPros ) return default;

            PropertyInfo[] properties = objectF.GetProperties();
            FieldInfo[] fields = objectF.GetFields();

            T resultObject = Activator.CreateInstance<T>();

            int indexCounter = 0;
            foreach ( PropertyInfo property in properties )
            {
                property.SetParseValue(resultObject, values[indexCounter++]);
            }

            foreach ( FieldInfo field in fields )
            {
                field.SetParseValue(resultObject, values[indexCounter++]);
            }

            return resultObject;
        }
        public string Serialise(T objectT)
        {
            Type objectF = typeof(T);
            PropertyInfo[] properties = objectF.GetProperties();
            FieldInfo[] fields = objectF.GetFields();

            string[] values = new string[properties.Length + fields.Length];

            int indexCounter = 0;
            foreach ( PropertyInfo prop in properties )
            {
                values[indexCounter++] = prop.GetValue(objectT).ToString();
            }
            foreach ( FieldInfo field in fields )
            {
                values[indexCounter++] = field.GetValue(objectT).ToString();
            }

            StringBuilder builder = new StringBuilder(7);
            builder.AppendJoin("\t", values);

            if(separator != ".")
            {
                builder.Replace(",", ".");
            }

            builder.Replace("\t", separator);
            return builder.ToString();
        }
    }
}
