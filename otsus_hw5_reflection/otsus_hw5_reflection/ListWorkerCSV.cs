﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otsus_hw5_reflection
{
    internal class ListWorkerCSV<T>
        where T: class
    {
        private readonly string _fileName = "info.csv";
        private readonly WorkerCSV<T> _workerCSV;

        public ListWorkerCSV(string separator = ",")
        {
            _workerCSV = new WorkerCSV<T>(separator);
        }

        public void Serialise( List<T> objects)
        {
            StringBuilder builder = new StringBuilder();
            foreach ( T objectT in objects)
            {
                builder.AppendLine(_workerCSV.Serialise(objectT));
            }
            File.WriteAllText(_fileName, builder.ToString());
        }
        public List<T> Deserialise()
        {
            if ( !File.Exists(_fileName) )
            {
                File.WriteAllText(_fileName,"");
            }
            var lines = File.ReadLines(_fileName);

            List<T> odjectsF = new List<T>();
            T odjectT = null;
            foreach ( string line in lines )
            {
                odjectT = _workerCSV.Deserialise(line);
                if ( odjectT != null )
                {
                    odjectsF.Add(odjectT);
                }
            }
            return odjectsF;
        }        
    }
}
